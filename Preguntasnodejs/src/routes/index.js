const { query } = require('express');
const express = require('express');
const pool = require('../database');
const router = express.Router();

router.get('/', async (req, res) => {

    if(req.query.buscar)
    {
        const preguntas= await pool.query('SELECT * FROM preguntas WHERE categoria like ?', '%' + req.query.buscar + '%');
        console.log(preguntas)
     res.render('index', {preguntas});
    }
    else
    {
        const preguntas= await pool.query("SELECT * FROM preguntas");
        res.render('index', {preguntas});
    }
    
});

router.get('/etiquetas', async (req, res) => {
    res.render('etiquetas/etiq');
});

module.exports = router;