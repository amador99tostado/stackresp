const express = require('express');
const router = express.Router();



const pool = require('../database');
const p = require('../lib/passport');
const { isLoggedIn } = require('../lib/auth');






//ruta para mostrar vista de respuestas
var fkidpregunta;
router.get('/responder/:id', isLoggedIn, async(req, res)=>{
    const { id } = req.params;
    fkidpregunta=id;
    const resp= await pool.query('SELECT * FROM preguntas WHERE id = ?', [id]);
    const r= await pool.query('SELECT * FROM respuestas WHERE fkidpregunta = ?', [id]);
    const contar = await pool.query('SELECT COUNT(*) AS contarespuestas FROM respuestas WHERE fkidpregunta = ?', [id]);
    res.render('Preguntas/VerResponder', {resp:resp[0], r, contar:contar[0]});
});
router.post('/responder', isLoggedIn, async(req, res)=>{
    const {respuesta}= req.body;
    const imagen= req.file.originalname;
    const usuarior= req.user.username;
    const guardres={
        respuesta,
        imagen,
        fkidpregunta,
        usuarior
    }
    await pool.query('INSERT INTO respuestas set ?', [guardres]);
    req.flash('success', 'Comentario publicado');
    res.redirect('/');

});

router.post('/like', isLoggedIn, async(req, res)=>{
    

});

router.get('/hacerpregunta',(req, res)=>{
    res.render('Preguntas/HacerPregunta');
});


router.post('/hacerpregunta',isLoggedIn, async(req, res)=>{
    const {titulo, descripcion,  categoria}= req.body;
   var  usuariop=  req.user.username;
   var imagen= req.file.originalname;
    const newpre={
        titulo,
         descripcion,
          categoria,
         usuariop,
         imagen
    };
    
    await pool.query('INSERT INTO preguntas set ?', [newpre]);
    req.flash('success', 'Pregunta publicada correctamente');
    res.redirect('/');
});



//eliminar preguntas de perfil

    router.get('/eliminar/:id', async(req, res)=>{
        const { id }= req.params;
        await pool.query('DELETE FROM preguntas WHERE id = ?', [id]);
        const respuestasr= await pool.query('SELECT * FROM respuestas WHERE fkidpregunta = ?', [id]);
        if(respuestasr)
        {
            await pool.query('DELETE FROM respuestas WHERE fkidpregunta = ?', [id]);
        req.flash('success', 'Pregunta eliminada correctamente');
        res.redirect('/profile');
        }
        else
        {
            req.flash('success', 'Pregunta eliminada correctamente');
            res.redirect('/profile');
        }
       
    });

    //editar pregunta de perfil
    router.get('/editar/:id', async(req, res)=>{
        const{id}= req.params;
        const r= await pool.query('SELECT * FROM preguntas WHERE id = ?', [id]);

        res.render('Preguntas/editarpregunta', {r: r[0]});
    });

    router.post('/editar/:id', async(req, res)=>{
        const{id}= req.params;
        const {titulo, descripcion, categoria} = req.body;
        const imagen= req.file.originalname;
        var usuariop= req.user.username;
        const editpreg = {
            titulo,
             descripcion,
              categoria, 
              usuariop,
              imagen
        };
         await pool.query('UPDATE preguntas set ? WHERE id = ?', [editpreg, id]);
         req.flash('success', 'Pregunta actualizada correctamente');
        res.redirect('/profile');
    });
    router.get('/eliminarespuesta/:id', async(req, res)=>{
        const { id }= req.params;
        await pool.query('DELETE FROM respuestas WHERE idr = ?', [id]);
        req.flash('success', 'Respuesta eliminada correctamente');
        res.redirect('/profile');
    });



module.exports = router;