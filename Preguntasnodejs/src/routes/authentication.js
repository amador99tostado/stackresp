const express = require('express');
const router = express.Router();

const passport = require('passport');
const { isLoggedIn } = require('../lib/auth');
const pool = require('../database');

// SIGNUP
router.get('/signup', (req, res) => {
  res.render('auth/signup');
});

router.post('/signup', passport.authenticate('local.signup', {
  successRedirect: '/profile',
  failureRedirect: '/signup',
  failureFlash: true
}));

// SINGIN
router.get('/signin', (req, res) => {
  res.render('auth/signin');
});

router.post('/signin', (req, res, next) => {
  req.check('username', 'Username is Required').notEmpty();
  req.check('password', 'Password is Required').notEmpty();
  const errors = req.validationErrors();
  if (errors.length > 0) {
    req.flash('message', errors[0].msg);
    res.redirect('/signin');
  }
  passport.authenticate('local.signin', {
    successRedirect: '/profile',
    failureRedirect: '/signin',
    failureFlash: true
  })(req, res, next);
});

router.get('/logout', (req, res) => {
  req.logOut();
  res.redirect('/');
});

router.get('/profile', isLoggedIn, async(req, res) => {
  const contar = await pool.query('SELECT COUNT(*) AS contarmispreguntas FROM preguntas WHERE usuariop = ?', [req.user.username]);
  const contarr = await pool.query('SELECT COUNT(*) AS contarmisrespuestas FROM respuestas WHERE usuarior = ?', [req.user.username]);
  const preguntas= await pool.query('SELECT * FROM preguntas WHERE usuariop = ?', [req.user.username]);
  const respuestas= await pool.query('SELECT p.id, p.titulo, r.idr, r.respuesta, r.imagen, p.usuariop FROM respuestas r, preguntas p WHERE r.fkidpregunta = p.id and usuarior = ?', [req.user.username]);
 res.render('profile', {contar:contar[0], contarr:contarr[0], preguntas, respuestas})
});

router.get('/editarperfil', isLoggedIn, async(req, res)=>{
  console.log(req.user.username);
  //const datos= await pool.query('SELECT * FROM users WHERE username = ?' [req.user.username]);
  const datos= await pool.query('SELECT * FROM users WHERE username = ?', [req.user.username]);
  res.render('auth/editarUsuario', {datos:datos[0]});
});

router.post('/editarperfil', isLoggedIn, async(req, res)=>{
    const {fullname, password}= req.body;
    const imagen= req.file.originalname;
    const editaruser={
      fullname, 
      password, 
      imagen
    };
    await pool.query('UPDATE users set ? WHERE id = ?', [editaruser, req.user.id]);
    req.flash('success', 'Datos de usuario actualizados correctamente');
    res.redirect('profile');
});

router.get('/usuarios', async(req, res)=>{
    const usuarios= await pool.query('SELECT * FROM users');
    console.log(usuarios);
    res.render('auth/usuarios', {usuarios});
});

module.exports = router;
